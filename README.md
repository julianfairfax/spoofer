[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Spoofer
Spoofer is a command line tool to spoof your MAC address and scan your network 

## Supported Operating Systems
- Arch Linux
- Debian-based Linux distributions
- macOS
- RPM-based Linux distributions

## Usage

### Step 1

Download the latest version of Spoofer from the GitHub releases page.

You can also download it from the Arch, Debian, RPM, and Homebrew repositories that are now available [here](https://gitlab.com/julianfairfax/package-repo).

### Step 2

Unzip the download and open Terminal. Type `chmod +x` and drag the `spoofer.sh` script to Terminal, then hit enter.

## Building
If you're on Linux and you want to build Spoofer, you can do so by running the `build_deb.sh` script for Debian Linux and `build_arch.sh` script for Arch Linux.

### Make sure to replace these values before running any commands:
- `$maintainer_name` should be replaced with your email by running `export maintainer_name="<NAME>"` or replacing it yourself. If none is set, `Julian Fairfax` will be used.
- `$maintainer_email` should be replaced with your email by running `export maintainer_email="<EMAIL>"` or replacing it yourself. If none is set, `juliannfairfax@protonmail.com`  will be used.

## Licensing information
The [mac_address.txt](resources/mac_addresses.txt) file is sourced from the [Institute of Electrical and Electronics Engineers](https://regauth.standards.ieee.org/standards-ra-web/pub/view.html#registries). I am unsure of the exact file and license for it.
