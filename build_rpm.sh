#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian Fairfax"
fi
if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="spoofer"
version="2.4.7"
description="Command line tool to spoof your MAC address and scan your network"

mkdir -p rpmbuild/RPMS/noarch rpmbuild/SOURCES rpmbuild/SPECS rpmbuild/SRPMS

echo "Summary: "$description"
Name: "$name"
Version: "$version"
Release: "$version"
License: GPL
URL: https://gitlab.com/julianfairfax/"$name"
Group: System
Packager: "$maintainer_name"
BuildRoot: "$(realpath ./rpmbuild)"

%description
"$description"

%prep
mkdir -p \$RPM_BUILD_ROOT/usr/bin/

cp "$(realpath $name)".sh \$RPM_BUILD_ROOT/usr/bin/"$name"
chmod +x \$RPM_BUILD_ROOT/usr/bin/"$name"

exit

%files
%attr(0744, root, root) /usr/bin/*

%clean
rm -rf \$RPM_BUILD_ROOT/usr/bin" | tee rpmbuild/SPECS/"$name".spec

cd rpmbuild/SPECS/
rpmbuild --target noarch -bb "$name".spec