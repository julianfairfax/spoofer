#!/bin/bash

parameters="${1}${2}${3}${4}${5}${6}${7}${8}${9}"

Platform_Variables()
{
	if [[ $(uname) == "Darwin" ]]; then
		if [[ $(sw_vers -productName) == "Mac OS X" || $(sw_vers -productName) == "macOS" ]]; then
			platform="macOS"
		fi
	fi

	if [[ $(uname) == "Linux" ]]; then
		platform="linux"
	fi
}

Escape_Variables()
{
	text_progress="\033[38;5;113m"
	text_success="\033[38;5;113m"
	text_warning="\033[38;5;221m"
	text_error="\033[38;5;203m"
	text_message="\033[38;5;75m"

	text_bold="\033[1m"
	text_faint="\033[2m"
	text_italic="\033[3m"
	text_underline="\033[4m"

	erase_style="\033[0m"
	erase_line="\033[0K"

	move_up="\033[1A"
	move_down="\033[1B"
	move_foward="\033[1C"
	move_backward="\033[1D"
}

Parameter_Variables()
{
	if [[ $parameters == *"-v"* ||  $parameters == *"-verbose"* ]]; then
		verbose="1"
		set -x
	fi

	if [[ $parameters == *"-daemon"* ]]; then
		daemon="1"
		daemon_option="1"
	fi
}

Path_Variables()
{
	script_path="${0}"
	directory_path="${0%/*}"

	resources_path="$directory_path/resources"

	if [[ "$daemon" == "1" && -d /usr/local/spoofer ]]; then
		resources_path="/usr/local/spoofer"
	fi

	if [[ "$daemon" == "1" && -d /opt/spoofer ]]; then
		resources_path="/opt/spoofer"
	fi

	if [[ -d /opt/spoofer ]]; then
		resources_path="/opt/spoofer"
	fi

	if [[ -d /usr/local/opt/spoofer ]]; then
		resources_path="/usr/local/opt/spoofer"
	fi
}

Input_Off()
{
	stty -echo
}

Input_On()
{
	stty echo
}

Output_Off()
{
	if [[ $verbose == "1" ]]; then
		"$@"
	else
		"$@" &>/dev/null
	fi
}

Check_Environment()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system environment."${erase_style}

	if [ -d /Install\ *.app ]; then
		environment="installer"
	else
		environment="system"
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system environment."${erase_style}
}

Check_Root()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for root permissions."${erase_style}

	if [[ $environment == "installer" ]]; then
		root_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
	else

		if [[ $(whoami) == "root" && $environment == "system" ]]; then
			root_check="passed"
			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
		else
			root_check="failed"
			echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Root permissions check failed."${erase_style}
			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with root permissions."${erase_style}
			Input_On
			exit
		fi

	fi
}

Check_Resources()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for resources."${erase_style}

	if [[ -d "$resources_path" ]]; then
		resources_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Resources check passed."${erase_style}
	else
		resources_check="failed"
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Resources check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with the required resources."${erase_style}

		Input_On
		exit
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Resources check passed."${erase_style}
}

Install_Packages()
{
	if [[ $platform == "linux" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Installing packages."${erase_style}

			if [[ ! -z "$(which apt)" ]]; then
				export DEBIAN_FRONTEND=noninteractive

				if [[ -z "$(which arp)" || -z "$(which ifconfig)" ]]; then
					Output_Off apt install net-tools -y
				fi

				if [[ -z "$(which ethtool)" ]]; then
					Output_Off apt install ethtool -y
				fi

				if [[ -z "$(which curl)" ]]; then
					Output_Off apt install curl -y
				fi
			fi

			if [[ ! -z "$(which pacman)" ]]; then
				if [[ -z "$(which arp)" || -z "$(which ifconfig)" ]]; then
					Output_Off pacman -S --needed --noconfirm net-tools
				fi

				if [[ -z "$(which ethtool)" ]]; then
					Output_Off pacman -S --needed --noconfirm ethtool
				fi

				if [[ -z "$(which curl)" ]]; then
					Output_Off pacman -S --needed --noconfirm curl
				fi
			fi

			if [[ ! -z "$(which dnf)" ]]; then
				if [[ -z "$(which arp)" || -z "$(which ifconfig)" ]]; then
					Output_Off dnf -q -y install --allowerasing net-tools
				fi

				if [[ -z "$(which ethtool)" ]]; then
					Output_Off dnf -q -y install --allowerasing ethtool
				fi

				if [[ -z "$(which curl)" ]]; then
					Output_Off dnf -q -y install --allowerasing curl
				fi
			fi

		echo -e $(date "+%b %d %H:%M:%S") ${text_success}"+ Installed packages."${erase_style}
	fi
}

Input_Interface()
{
	if [[ $platform == "linux" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What interface would you like to use?"${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an interface."${erase_style}
		
		intrl_number="0"

		while read intrl_option; do
			intrl_number=$(($intrl_number + 1))
			declare intrl_$intrl_number="$intrl_option"

			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     ${intrl_number} - ${intrl_option}"${erase_style} | sort

			daemon_option_number="$intrl_number"
		done <<<"$(ls /sys/class/net | sed 's/\  /\n/')"

		if [[ "$daemon" == "1" && ! $daemon_option -gt $daemon_option_number ]]; then
			intrl_choosen_number="$daemon_option"
			intrl_choosen="intrl_$intrl_choosen_number"
			intrl="${!intrl_choosen}"
		else
			Input_On
			read -e -p "$(date "+%b %d %H:%M:%S") / " intrl_choosen_number
			Input_Off
			intrl_choosen="intrl_$intrl_choosen_number"
			intrl="${!intrl_choosen}"
		fi
	fi

	if [[ $platform == "macOS" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What interface would you like to use?"${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an interface number."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     1 - Ethernet"${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     2 - WiFi"${erase_style}

		daemon_option_number="2"

		if [[ "$daemon" == "1" && ! $daemon_option -gt $daemon_option_number ]]; then
			intrl="$daemon_option"
		else
			Input_On
			read -e -p "$(date "+%b %d %H:%M:%S") / " intrl
			Input_Off
		fi

		if [[ $intrl == "1" ]]; then
			en="en0"
			eth="eth0"
		fi

		if [[ $intrl == "2" ]]; then
			en="en1"
			eth="wlan0"
		fi
	fi
}

Input_Operation()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What operation would you like to run?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an operation number."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     1 - Check MAC address"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     2 - Spoof MAC address"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     3 - Reset MAC address"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     4 - Quick network scan"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     5 - Deep network scan"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     6 - Install Spoofer in background"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     7 - Uninstall Spoofer in background"${erase_style}

	if [[ "$daemon" == "1" && ! $daemon_option -gt $daemon_option_number ]]; then
		operation="2"
	else
		Input_On
		read -e -p "$(date "+%b %d %H:%M:%S") / " operation
		Input_Off
	fi

	Install_Packages
	
	if [[ $operation == "1" ]]; then
		Current_Address
		Check_Address
	fi

	if [[ $operation == "2" || $operation == "3" ]]; then
		Current_Address
		Modify_Address
	fi

	if [[ $operation == "4" || $operation == "5" ]]; then
		List_Devices
	fi

	if [[ $operation == "6" ]]; then
		Install_Spoofer
	fi

	if [[ $operation == "7" ]]; then
		Uninstall_Spoofer
	fi
}

Install_Spoofer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Installing Spoofer in background."${erase_style}

	if [[ $platform == "macOS" ]]; then
		if [[ -f /Library/LaunchDaemons/com.rmc.spoofer.plist ]]; then
			rm /Library/LaunchDaemons/com.rmc.spoofer.plist
		fi

		cp -R "$resources_path" /usr/local/spoofer

		echo -e "<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.julian-fairfax.spoofer</string>
    <key>ProgramArguments</key>
    <array>
        <string>/bin/bash</string>
        <string>/usr/local/bin/spoofer</string>
        <string>-daemon</string>
    </array>
    <key>KeepAlive</key>
    <dict>
        <key>Crashed</key>
        <true/>
    </dict>
    <key>RunAtLoad</key>
    <true/>
</dict>
</plist>" > /Library/LaunchDaemons/com.julian-fairfax.spoofer.plist
		
		cp "$script_path" /usr/local/bin/spoofer
		chmod +x /usr/local/bin/spoofer
	fi

	if [[ $platform == "linux" ]]; then

		if [[ ! -d /opt/spoofer ]]; then
			cp -R "$resources_path" /opt/spoofer
		fi

		echo "[Unit]
Description=spoofer

[Service]
ExecStart=/usr/bin/spoofer -daemon

[Install]
WantedBy=multi-user.target" | Output_Off tee /etc/systemd/system/spoofer.service

		Output_Off systemctl enable spoofer.service

		if [[ ! -d /usr/bin/spoofer ]]; then
			cp "$script_path" /usr/bin/spoofer
			chmod +x /usr/bin/spoofer
		fi
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Installed Spoofer in background."${erase_style}
}

Uninstall_Spoofer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Uninstalling Spoofer in background."${erase_style}

		if [[ $platform == "macOS" ]]; then
			rm -R /usr/local/spoofer

			rm /Library/LaunchDaemons/com.rmc.spoofer.plist

			rm /usr/local/bin/spoofer
		fi

		if [[ $platform == "linux" ]]; then
			rm /etc/systemd/system/spoofer.service
		fi
		
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Uninstalled Spoofer in background."${erase_style}
}

Current_Address()
{
	if [[ $platform == "linux" ]]; then
		mac_current="$(ip link show $intrl | sed 1d | sed 's/.*ether\ //' | sed 's/\ .*//')"
		mac_hardware="$(ethtool -P $intrl | sed 's/.*Permanent\ address:\ //')"
	fi
	
	if [[ $platform == "macOS" ]]; then
		mac_current="$(ifconfig $en | grep ether | sed 's/.*ether\ //')"
		mac_hardware="$(networksetup -getmacaddress $en | sed 's/.*Address:\ //' | sed 's/\ (Device:.*)//')"
	fi
}

Random_Address()
{
	if [[ $platform == "linux" ]]; then	
		mac_registered="$(shuf "$resources_path"/mac_addresses.txt | head -n 1 | sed 's/../&:/g;s/:$//' | cut -c 1-8)"
	fi

	if [[ $platform == "macOS"  ]]; then
		mac_registered="$(sort -R "$resources_path"/mac_addresses.txt | head -n 1 | sed 's/../&:/g;s/:$//' | cut -c 1-8)"
	fi

	mac_random="$(hexdump -n 3 -v -e '3/1 "%02X" "\n"' /dev/urandom | sed 's/../&:/g;s/:$//')"
}

Check_Address()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Current MAC address is $mac_current"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Hardware MAC address is $mac_hardware"${erase_style}
}

Modify_Address()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Modifying MAC address."${erase_style}

		if [[ $operation == "2" ]]; then
			Random_Address
			mac_set="$mac_registered:$mac_random"
		fi

		if [[ $operation == "3" ]]; then
			mac_set="$mac_hardware"
		fi

		if [[ $platform == "linux" ]]; then
			ifconfig $intrl down
			ip link set $intrl address $mac_set
			mac_new="$(ip link show $intrl | sed 1d | sed 's/.*ether\ //' | sed 's/\ .*//')"
			ifconfig $intrl up
		fi

		if [[ $platform == "macOS" ]]; then
			ifconfig $en ether $mac_set
			mac_new="$(ifconfig $en | grep ether | sed 's/.*ether\ //')"
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Modified MAC address."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"Old MAC address was $mac_current"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"New MAC address is $mac_new"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"Hardware MAC address is $mac_hardware"${erase_style}

	if [[ "$daemon" == "1" && $daemon_option -lt "$daemon_option_number" ]]; then		
		daemon_option="$(($daemon_option + 1))"

		Input_Interface
		Input_Operation
	fi
}

List_Devices()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Scanning network."${erase_style}

		if [[ $operation == "5" ]]; then
			if [[ $platform == "linux" ]]; then
				network_ip="$(ifconfig $intrl | grep inet\  | sed 's/.*inet\ //' | sed 's/\  netmask.*//' | cut -d"." -f1,2,3)"
			fi

			if [[ $platform == "macOS" ]]; then
				network_ip="$(ifconfig $en | grep inet\  | sed 's/.*inet\ //' | sed 's/\ netmask.*//' | cut -d"." -f1,2,3)"
			fi

			network_counter=1

			while [[ "$network_counter" -lt "254" ]]; do
			   Output_Off ping -c 1 -W 1 $network_ip.$network_counter
			   network_counter=$(( $network_counter + 1 ))
			done
		fi

		device_list="$(arp -a | sort -n | sed 's/?\ //')"

		if [[ $platform == "linux" ]]; then
			device_list="$(echo "$device_list" | grep -v "<incomplete>" | sed "s/.*(/$(date "+%b %d %H:%M:%S")\ /" | sed 's/)//' | sed 's/\[ether\].*on/on/' | sed 's/\ on.*//')"
		fi

		if [[ $platform == "macOS" ]]; then
			device_list="$(echo "$device_list" | grep -v "(incomplete)" | sed "s/.*(/$(date "+%b %d %H:%M:%S")\ /" | sed 's/)//' | sed 's/\ on.*//')"
		fi

		while read device; do
			device_mac="$(echo $device | sed 's/.*at\ //')"
			device_vendor="$(cat "$resources_path"/mac_addresses.txt | grep $(echo $device_mac | cut -c 1-8 | sed 's/://g' | sed 's/[a-z]/\U&/g') | sed 's/.*\t//')"

			if [[ -z "$device_vendor" ]]; then
				device_echo="$device_echo\n$(echo $device)"
			else
				device_echo="$device_echo\n$(echo $device) from $device_vendor"
			fi
		done <<<"$device_list"

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Scanned network."${erase_style}


	echo -e "$(echo -e "$device_echo" | sed 1d)"
}

End()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Thank you for using Spoofer."${erase_style}
	
	Input_On
	exit
}

Input_Off
Platform_Variables
Escape_Variables
Parameter_Variables
Path_Variables
Check_Environment
Check_Root
Check_Resources
Input_Interface
Input_Operation
End
