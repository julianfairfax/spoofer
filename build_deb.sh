#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian Fairfax"
fi
if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="spoofer"
version="2.4.7"
description="Command line tool to spoof your MAC address and scan your network"

install -d $name/usr/bin

cp $name.sh $name/usr/bin/$name

chmod +x $name/usr/bin/$name
mkdir $name/DEBIAN

echo "Package: $name
Version: $version
Architecture: all
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee $name/DEBIAN/control

dpkg-deb -Z xz -b $name/ .

rm -r $name